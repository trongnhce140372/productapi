import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'ProductApp';

  isShow = false;
  isHide = true;

  toggleDisplay(){
    this.isHide = !this.isHide;
    this.isShow = !this.isShow;
  }
}
