import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  isShow = false;
  isHide = true;

  toggleDisplay(){
    this.isShow = !this.isShow;
    this.isHide = !this.isHide;
  }

  ngOnInit(): void {
  }

}
