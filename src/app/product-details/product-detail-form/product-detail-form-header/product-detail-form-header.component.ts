import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-detail-form-header',
  templateUrl: './product-detail-form-header.component.html',
  styleUrls:['./product-detail-form-header.css']
})
export class ProductDetailFormHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
