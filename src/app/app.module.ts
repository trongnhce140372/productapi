import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductDetailFormComponent } from './product-details/product-detail-form/product-detail-form.component';
import { ProductDetailFormTopComponent } from './product-details/product-detail-form/product-detail-form-top/product-detail-form-top.component';
import { ProductDetailFormBottomComponent } from './product-details/product-detail-form/product-detail-form-bottom/product-detail-form-bottom.component';
import { ListProductsComponent } from './list-products/list-products.component';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { ProductDetailFormHeaderComponent } from './product-details/product-detail-form/product-detail-form-header/product-detail-form-header.component';
import { ListProductImageComponent } from './list-product-image/list-product-image.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  {
    path: 'list',
    component: ListProductsComponent,
  },
  {
    path: 'list-image',
    component: ListProductImageComponent,
  },
  {
    path: 'product',
    component: ProductDetailsComponent,
  }];
@NgModule({
  declarations: [
    AppComponent,
    ProductDetailsComponent,
    ProductDetailFormComponent,
    ProductDetailFormTopComponent,
    ProductDetailFormBottomComponent,
    ListProductsComponent,
    HeaderComponent,
    ProductDetailFormHeaderComponent,
    ListProductImageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
