 function list_js() {
     document.addEventListener('DOMContentLoaded', function() {
         const table = document.getElementById('table');

         let draggingEle;
         let draggingRowIndex;
         let placeholder;
         let list;
         let isDraggingStarted = false;


         let x = 0;
         let y = 0;


         const swap = function(nodeA, nodeB) {
             const parentA = nodeA.parentNode;
             const siblingA = nodeA.nextSibling === nodeB ? nodeA : nodeA.nextSibling;


             nodeB.parentNode.insertBefore(nodeA, nodeB);


             parentA.insertBefore(nodeB, siblingA);
         };


         const isAbove = function(nodeA, nodeB) {

             const rectA = nodeA.getBoundingClientRect();
             const rectB = nodeB.getBoundingClientRect();

             return (rectA.top + rectA.height / 2 < rectB.top + rectB.height / 2);
         };

         const cloneTable = function() {
             const rect = table.getBoundingClientRect();
             const width = parseInt(window.getComputedStyle(table).width);

             list = document.createElement('div');
             list.classList.add('clone-list');
             list.style.position = 'absolute';
             list.style.left = `${rect.left}px`;
             list.style.top = `${rect.top}px`;
             table.parentNode.insertBefore(list, table);

             // Hide the original table
             table.style.visibility = 'hidden';

             table.querySelectorAll('tr').forEach(function(row) {

                 const item = document.createElement('div');
                 item.classList.add('draggable');

                 const newTable = document.createElement('table');
                 newTable.setAttribute('class', 'clone-table');
                 newTable.style.width = `${width}px`;

                 const newRow = document.createElement('tr');
                 const cells = [].slice.call(row.children);
                 cells.forEach(function(cell) {
                     const newCell = cell.cloneNode(true);
                     newCell.style.width = `${parseInt(window.getComputedStyle(cell).width)}px`;
                     newRow.appendChild(newCell);
                 });

                 newTable.appendChild(newRow);
                 item.appendChild(newTable);
                 list.appendChild(item);
             });
         };

         const mouseDownHandler = function(e) {

             const originalRow = e.target.parentNode;
             draggingRowIndex = [].slice.call(table.querySelectorAll('tr')).indexOf(originalRow);


             x = e.clientX;
             y = e.clientY;


             document.addEventListener('mousemove', mouseMoveHandler);
             document.addEventListener('mouseup', mouseUpHandler);
         };

         const mouseMoveHandler = function(e) {
             if (!isDraggingStarted) {
                 isDraggingStarted = true;

                 cloneTable();

                 draggingEle = [].slice.call(list.children)[draggingRowIndex];
                 draggingEle.classList.add('dragging');


                 placeholder = document.createElement('div');
                 placeholder.classList.add('placeholder');
                 draggingEle.parentNode.insertBefore(placeholder, draggingEle.nextSibling);
                 placeholder.style.height = `${draggingEle.offsetHeight}px`;
             }


             draggingEle.style.position = 'absolute';
             draggingEle.style.top = `${draggingEle.offsetTop + e.clientY - y}px`;
             draggingEle.style.left = `${draggingEle.offsetLeft + e.clientX - x}px`;


             x = e.clientX;
             y = e.clientY;


             const prevEle = draggingEle.previousElementSibling;
             const nextEle = placeholder.nextElementSibling;


             if (prevEle && prevEle.previousElementSibling && isAbove(draggingEle, prevEle)) {

                 swap(placeholder, draggingEle);
                 swap(placeholder, prevEle);
                 return;
             }


             if (nextEle && isAbove(nextEle, draggingEle)) {

                 swap(nextEle, placeholder);
                 swap(nextEle, draggingEle);
             }
         };

         const mouseUpHandler = function() {

             placeholder && placeholder.parentNode.removeChild(placeholder);

             draggingEle.classList.remove('dragging');
             draggingEle.style.removeProperty('top');
             draggingEle.style.removeProperty('left');
             draggingEle.style.removeProperty('position');


             const endRowIndex = [].slice.call(list.children).indexOf(draggingEle);

             isDraggingStarted = false;


             list.parentNode.removeChild(list);


             let rows = [].slice.call(table.querySelectorAll('tr'));
             draggingRowIndex > endRowIndex ?
                 rows[endRowIndex].parentNode.insertBefore(rows[draggingRowIndex], rows[endRowIndex]) :
                 rows[endRowIndex].parentNode.insertBefore(rows[draggingRowIndex], rows[endRowIndex].nextSibling);

             // Bring back the table
             table.style.removeProperty('visibility');

             // Remove the handlers of `mousemove` and `mouseup`
             document.removeEventListener('mousemove', mouseMoveHandler);
             document.removeEventListener('mouseup', mouseUpHandler);
         };

         table.querySelectorAll('tr').forEach(function(row, index) {

             if (index === 0) {
                 return;
             }

             const firstCell = row.firstElementChild;
             firstCell.classList.add('draggable');
             firstCell.addEventListener('mousedown', mouseDownHandler);
         });
     });
 }
